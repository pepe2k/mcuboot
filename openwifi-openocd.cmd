set WORKAREASIZE 0x4000
source [find "$ADAPTER"]
transport select swd
source [find target/nrf52.cfg]
init
targets
reset init
nrf5 mass_erase
flash write_image "$IMAGE"
reset init
verify_image "$IMAGE"
reset run
shutdown
